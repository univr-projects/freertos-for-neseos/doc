The purpose of this document is to show how to implement an Ethernet real-time communication FreeRTOS-based application for an LPC platform. 

In the \textit{Introduction} chapter there is an overview of the current implementation of an Ethernet protocol stack for Embedded System and all steps that brought the realization of this project and the porting of an Ethernet protocol stack for a specific Embedded device, in particular the FreeRTOS+TCP stack for an LPC1768 device. 

After that, in the \textit{Networking Glossary} chapter, there is a brief list of all the concepts useful to understand all the components contained in an Ethernet protocol stack. 

Then, in the \textit{FreeRTOS+TCP Porting} chapter, there is explained how FreeRTOS+TCP stack has been ported in the project, the implementations and the protocol stack modifications needed to make the firmware compatible with the platform chosen.

Finally, in the \textit{Future Improvements} chapter there are some suggestions to improve the project and, in \textit{Conclusions} chapter, the results and critical issues found in the project development. 

The project goals are:
\begin{itemize}
	\item Configure and implement an Ethernet communication with an embedded device, using FreeRTOS and FreeRTOS+TCP protocol stack;
	\item Use FreeRTOS real-time API and implement multiple concurrent tasks, with Ethernet interactions and real-time constraints; 
\end{itemize}


%========================================================
\section{Platforms available}
The platforms that we have adopted are a \textit{mbed NXP LPC} platform \textit{LPC1768} model and a \textit{FREESCALE FREEDOM} platform \textit{FRDM-KL25Z} model. In the following list there is an explanation of each board features:

\begin{itemize}
	\item \textbf{mbed NXP LPC1768}:
	\begin{itemize}
		\item microcontroller: Cortex-M3 core, 96MHz, 32KB RAM, 512KB FLASH;
		\item firmware: Mbed firmware 141212 version;
		\item tools: Mbed Studio, PlatformIO, MCUXpresso IDE;
		\item library: \href{https://www.nxp.com/design/microcontrollers-developer-resources/lpcopen-libraries-and-examples/lpcopen-software-development-platform-lpc17xx:LPCOPEN-SOFTWARE-FOR-LPC17XX}{OpenLPC library} included in MCUXpresso IDE;
		\item os: mbed-os or FreeRTOS (not both together);
		\item others: Ethernet, USB Host/Device, 2xSPI, 2xI2C, 3xUART, CAN, 6xPWM, 6xADC, GPIO;
		\item project reference: \href{https://gitlab.com/univr-projects/freertos-for-neseos/mbedlpc1768}{mbedLPC1768};
	\end{itemize}
	\item \textbf{FREESCALE FREEDOM FRDM-KL25Z}: 
	\begin{itemize}
		\item microcontroller: MKL25Z128VLK4 MCU, 48 MHz, 128 KB flash, 16 KB SRAM;
		\item firmware: OpenSDA bootloader 1.11 version and OpenSDA firmware 118 version, but it is also compatible with Mbed firmware;
		\item tools: MCUXpresso IDE and SDK, Mbed Studio, PlatformIO;
		\item library: MCUXpresso SDK \verb|MKL25Z128xxx4|;
		\item os: mbed-os, FreeRTOS included in MCUXpresso SDK, ZephyrOS;
		\item others: LCD display, doesn't support Ethernet (it needs a shield or an extension board);
		\item project reference: \href{https://gitlab.com/univr-projects/freertos-for-neseos/frdm-kl25z}{FRDM-KL25Z}
	\end{itemize}
\end{itemize} 


%========================================================
\section{Ethernet Protocol Stacks}
There exists different protocol stacks that can be used to implement an Ethernet communication, TCP or UDP, in a network. In particular there are 3 main Ethernet protocol stack: lwIP, uIP and FreeRTOS+TCP. There are also other protocol stacks, but they were of minor importance or they were intermediate implementations that quickly turned into a new implementation. \href{https://www.freertos.org/embeddedtcp.html}{Here} you can find a reference for some protocol stacks historically used with FreeRTOS operating system before the creation of FreeRTOS+TCP and some example demos.

The \textbf{uIP} protocol stack runs in a single task, therefore it is not compatible with FreeRTOS multitasking, but there are no concurrence issues to be managed. For this meaning, uIP is a protocol stack really light, it allows a lot to be achieved in a very small RAM and ROM footprint, and could be a great piece of software when it is used in an appropriate application. However there is a biggest drawback, that is, you can only have one packet outstanding on the network at any time, and this behaviour makes the communication slower.

\textbf{FreeTCPIP} is the modified version of uIP, made by FreeRTOS team, that improves the throughput performance, but with the tradeoff of increased RAM and CPU usage. However the throughput obtained is still slower than that achievable by a more fully featured stack, but it was more than adequate for many embedded microcontroller based applications.

The \textbf{lwIP} protocol stack is also a good stack when used in an intended, memory constrained, environment. It has a higher throughput than uIP, but also has a larger ROM and RAM footprint, and it's still slower than a fully featured stack. On the negative side, lwIP is quite complex to use at first, but time invested in its use will pay in future projects.

\textbf{FreeRTOS+TCP} is a fairly new extension library of FreeRTOS operating system, scalable, open source and thread safe, that integrate TCP/IP stack in FreeRTOS. It provides a familiar and standard socket interface, making it as simple to use and as quick to learn as possible. The protocol stack's features and RAM footprint are fully scalable, making FreeRTOS+TCP equally applicable to smaller microcontrollers as larger microprocessors. Nowadays it is recommended that all new network enabled platform, using an open source TCP/IP stack, use FreeRTOS+TCP.

\textbf{FreeRTOS+UDP} was a small, fully thread aware, sockets based, and very efficient UDP/IP (IPv4) stack, but now this FreeRTOS extension library is fully integrated in FreeRTOS+TCP, which can be configured UDP only use, as alternative.

The Ethernet protocol stack chosen for this project is FreeRTOS+TCP, because it's the most recent and, since it is a FreeRTOS operating system extension library, it's the most compatible with FreeRTOS multitasking and real-time features. Moreover FreeRTOS+TCP is the most portable Ethernet protocol stack compared to the other ones, therefore if someone will want to build this project for another platform, it will have to change and implement only the porting functions of the library, without changing the main behaviour of the implementation. FreeRTOS+TCP is also really configurable, in fact you can switch between TCP and UDP communication using the same API, you can change network communication features and platform behaviour editing only a configuration file, the \textit{FreeRTOSIPConfig.h} header file. For example you can edit the RX and TX buffer lengths, the time to live (TTL), the maximum segment size (MSS), and so on (\href{https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/TCP_IP_Configuration.html}{here} for more info about the \textit{FreeRTOSIPConfig.h} header file). 


%========================================================
\section{Project evolution steps}
The project was done in different steps: an exploration phase, in which we understand what was the best embedded platform to use to build a real-time application with FreeRTOS and an Ethernet communication, and then 3 steps in which we choose the best Ethernet protocol stack to use, we configure the porting for the protocol stack chosen and finally we implement a first application establishing a communication on an Ethernet network. 

The \textbf{exploration phase} was needed to understand what are the development tools to use with a \textit{mbed NXP LPC1768} platform and the \textit{FREESCALE FREEDOM FRDM-KL25Z} platform. The FRDM-KL25Z platform was tested only with NXP tools, in particular MCUXpresso IDE, but it is also compatible with Mbed tools. It needs the related MCUXpresso SDK, in particular the \verb|MKL25Z128xxx4| package, and it works with FreeRTOS, that can be imported through the SDKs manager accessible in MCUXpresso IDE. The LPC1768 was tested with both Mbed and NXP tools, but we have chosen MCUXpresso because Mbed tools are not compatible with FreeRTOS kernel, then we were forced to use the NXP environments. The LPC platform doesn't use the MCUXpresso SDK, but the LPCOpen library that is included in MCUXpresso IDE. In the end we opted for the LPC platform, because the Freescale FRDM-KL25Z doesn't support, without shield or board extensions, an Ethernet connection which instead is supported by default in LPC1768. 

From this point, the platform that was integrated with the Ethernet protocol stack is the LPC1768. If someone in future will want, for some reason, to get back on the Freescale FRDM-KL25Z, you have first to take care of the bootloader version that is running in the board because some old FRDM-KL25Z platform have installed by default an 1.10 bootloader version or less and if you enter in bootloader mode with a modern operating system, you brick the platform. The only operating system that can upgrade the bootloader platform when its version is 1.10 or less are Windows XP or Windows 7 (no virtual machines). Therefore if the bootloader version is 1.11 or more, you should not have problems with bootloader access and firmware upgrade, otherwise do not enter the platform bootloader, or if you have to do it, because you have to upgrade the platform bootloader or firmware, get a Windows XP or Windows 7 computer. If your board doesn't work any more after previously bootloader access, probably the problem is this one. Take a look \href{https://gitlab.com/univr-projects/freertos-for-neseos/frdm-kl25z}{here} for the fix and for additional info. 

In a \textbf{second phase}, we started to see what is the Ethernet protocol stack state of art for Embedded Systems. We analysed different Ethernet implementation stacks, with the help of some implementations demos, included in \href{https://www.nxp.com/design/microcontrollers-developer-resources/lpcopen-libraries-and-examples/lpcopen-software-development-platform-lpc17xx:LPCOPEN-SOFTWARE-FOR-LPC17XX}{OpenLPC library} and \href{https://www.freertos.org/embeddedtcp.html}{FreeRTOS+TCP resources}, to understand what is the best solution to use nowadays and for our LPC1768. The protocols studied are summarized in the previous section and, considering that we were searching for a FreeRTOS implementation and in order to comply with real-time behaviour that only a operating system like FreeRTOS can realize, we have chosen FreeRTOS+TCP protocol stack. Moreover FreeRTOS+TCP is the most compatible with FreeRTOS, being an extension of it, and, at the moment, it's the most recommended for all new network enabled projects.

In a \textbf{third phase} we implemented the FreeRTOS+TCP library porting for LPC1768 device. This step is really important because it is the meaning why this Ethernet protocol stack library is so portable and scalable. FreeRTOS+TCP library is made to be compatible with many embedded platforms, with different architecture, cross-compiler, firmware, debugger, etc. In order to build your application with FreeRTOS+TCP you have to implement only few functions, that, in brief, call the lower protocol layer, the MAC layer. In the case of LPC1768 the MAC protocol layer is implemented in a driver included in OpenLPC library named \textit{EMAC driver} (Ethernet MAC driver). FreeRTOS has some porting implementations already done, in fact the LPC1768 porting was partially done, but it needed some adjustments, because the LPC1768 is quite old and it's currently not widely used, while FreeRTOS+TCP library is relatively new and daily updated. 

In a \textbf{fourth phase} we implemented a simple TCP Echo server and client application, running inside two concurred tasks. In this phase we started to test the project and try to run some demo and we understand that probably the LPC1768 is not compatible with MCUXpresso IDE tools. All the Ethernet demos stop the processor in a "Cannot halt processor" problem. After this error it becomes no longer possible to test any project on the LPC1768 with MCUXpresso, because trying to run any project, the error always return. The only way to recover the LPC1768 platform is to use Mbed Studio to build a \verb|.bin| execution file, and load it in the LPC1768. After power-cycle the board by disconnecting and reconnecting the cable, I can still use MCUXpresso to run projects. 

Unfortunately, for this problem, the project has not yet been tested, but the porting for the LPC1768 is done and if in future someone should find the solution at LPC1768 "Cannot halt processor" error, the project is ready to be testes. Otherwise, if you are going to use FreeRTOS+TCP for a different platform, in the succeeding sections there is explained how to do the FreeRTOS+TCP porting and additional useful information about the Ethernet protocol stack. Moreover you can use the project created for the LPC1768 itself as a template for a new project. 
# Documentation for FreeRTOS projects for NESEOS

Current projects for Network Embedded System and Embedded Operating System:
- **FRDM-KL25Z**: FREESCALE FREEDOM Platform FRDM-KL25Z with FreeRTOS;
- **mbedLPC1768**: Platform mbed LPC1768 with FreeRTOS;
